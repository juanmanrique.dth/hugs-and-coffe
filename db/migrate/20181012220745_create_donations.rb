class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.string :message
      t.string :occupation
      t.string :contributor
      t.references :donable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
