class CreateBeverages < ActiveRecord::Migration[5.2]
  def change
    create_table :beverages do |t|
      t.references :profile, foreign_key: true
      t.string :quantity

      t.timestamps
    end
  end
end
