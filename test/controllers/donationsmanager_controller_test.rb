require 'test_helper'

class DonationsmanagerControllerTest < ActionDispatch::IntegrationTest
  test "should get menu" do
    get donationsmanager_menu_url
    assert_response :success
  end

  test "should get thanks" do
    get donationsmanager_thanks_url
    assert_response :success
  end

end
