class Profile < ApplicationRecord
  belongs_to :user
  has_one_attached :avatar
  has_many :accounts
  has_many :images
  has_many :beverages
  # has_many :hugs
end
