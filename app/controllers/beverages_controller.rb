class BeveragesController < ApplicationController
  def new
    @profile = Profile.find(params[:profile_id])
    @beverage = Beverage.new
  end

  def create
    @profile = Profile.find(params[:profile_id])
    @beverage = @profile.beverages.build({quantity: params[:beverage][:quantity]})
    if @beverage.save
      @beverage.build_donation({
        occupation: params[:beverage][:occupation],
        message: params[:beverage][:message],
        contributor: params[:beverage][:contributor],
      }).save
      redirect_to donationsmanager_thanks_path(@profile), notice: 'Thank you for your donation!'
    else
      render :new
    end
  end
end
