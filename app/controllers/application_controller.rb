class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private

  def after_sign_in_path_for(resource)
    "/profiles/#{ current_user.profile.id }/edit"
  end
end
